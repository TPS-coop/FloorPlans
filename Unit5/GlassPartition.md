# Assembly instructions

Piotr Malachowicz gave some guidance as to how to set up the glass partition and doors.
 
## Materials needed

* At least 9 allen head chamfered screw heads. This can for sure be found in A and D Fastners in Chiswick. Unsure if the business is cash only.After the Chiswick fields underground, going down Bollow lane and later Colville Lane.
* A solution for Locking the doors:
    * Mag lock [Will need a clamp](https://www.locksonline.co.uk/Magnetic-Locks-/Magnetic-Door-Locks-for-Glass-Doors/Magnetic-Locks-Fixing-Brackets-for-Glass-Doors-.html) or [this clamp](http://www.easylocks.co.uk/ezlock-glass-door-maglock-bracket)  to fit with  a normal maglock such as [this one for example](https://www.locksonline.co.uk/Magnetic-Locks-/Magnetic-Direct-Pull-Locks.html) 
    * No hole needed lock? [Ben found.](http://www.barrier-components.co.uk/retro-fit-patch-lock-gl24-2.html)
    * Cabinet door lock or cubboard door lock to the bottom of the door
* Gloves, basic ones from B&Q are ok but better if rubbery since they attach to glass better. 
* OPTIONAL glass suction cups. If you doubt on the quality of the grip don't depend on them. The ones Piotr uses are more than [£100, example](https://www.amazon.co.uk/USAG-U04240103-424-A3-Multi-Purpose-Suction-Steel-Handle/dp/B01DWFP37E/ref=sr_1_1?s=industrial&rps=1&ie=UTF8&qid=1512757777&sr=1-1&keywords=suction+cups&refinements=p_36%3A118664031%2Cp_76%3A419159031) Note that each glass panel weighs more than 75 Kg  


## Size

Will need to match channels to doors. The important bit is the vertical side. Matching to door is more important than matching to partition. They are labeled 11, 12, 13. But there is an extra set of rails 6. 

* Number:6 2680mm
* Number:12 2520mm
* Number:11 2510mm (corresponds to the panel labeled 14)
* Number:13 2525mm (corresponds to the panel labeled 70k)


The door will restrict our vertical height. Frame should be at least 2525mm high. 

## Preparation

* Door frame as per restrictions on Door size.
* Hole in concrete floor to hold the door hinge in place. 

## Assembly

We need gloves: he recommends the ones found in B+Q that are rubbery. Better to use a tight fit. For example size 8 for Andres. 

Set the vertical rail first, later the right hand side rail and finally the bottom rail.The bottom rail has two components that will need to be assembled.  

The top and left side of the rubber seal will need to be on the rail. But the bottom bit will be set on the glass. 

First set up the fix panel. Top edge goes first on the top rail then slide to the right. Finally gently let the glass go down on to the bottom rail. 

The bottom rail can be raised with timber spacers to compensate for any tilt that the glass might have. Also to insure that the top part is firmly in the rail. The spacers should be set over the rubber.

Next is the door. Bottom and top brakets should be bolted on with the Allen bolts that have not been provided. It should be tight set on with no fear in breaking the glass. If after the door assembly looks a bit tilted then it is OK to disassemble and put a bit of plastic to ensure it is correct. The silver component from the top should be removed and assembled later. 

Door assembly should be done from the bottom first. Using a spacer (6mm tall?) to the loose corner of the glass to reduce tilt.  The silver component is set on the top bit (matching round hole with round pin) and after the glass comes to it to  match the height. 
 

